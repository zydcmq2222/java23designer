package com.surpass.designer.No15_桥接模式.bridgems;

import com.surpass.designer.No15_桥接模式.bridgems.control.SonyControl;
import com.surpass.designer.No15_桥接模式.bridgems.control.TvControl;

/**
 * 改装遥控器
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/3 23:57
 */
public class SonyTvControl extends SonyControl implements TvControl {

    private static int ch = 0;
    private static boolean ison = false;

    @Override
    public void Onoff() {
        if (ison) {
            ison = false;
            super.Off();
        } else {
            ison = true;
            super.On();
        }
    }

    @Override
    public void nextChannel() {
        ch++;
        super.setChannel(ch);
    }

    @Override
    public void preChannel() {
        ch--;
        if (ch < 0) {
            ch = 200;
        }
        super.setChannel(ch);
    }
}
