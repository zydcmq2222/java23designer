package com.surpass.designer.No15_桥接模式.bridgems.bridge;


import com.surpass.designer.No15_桥接模式.bridgems.control.LGControl;
import com.surpass.designer.No15_桥接模式.bridgems.control.SharpControl;
import com.surpass.designer.No15_桥接模式.bridgems.control.SonyControl;

/**
 * 采用桥接模式形成的主类测试
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 21:22
 */
public class MainTest {
    public static void main(String[] args) {
        TvControl mLGTvControl;
        TvControl mSonyTvControl;
        mSonyTvControl = new TvControl(new SonyControl());
        mLGTvControl = new TvControl(new LGControl());
        mLGTvControl.Onoff();
        mLGTvControl.nextChannel();
        mLGTvControl.nextChannel();
        mLGTvControl.preChannel();
        mLGTvControl.Onoff();

        mSonyTvControl.Onoff();
        mSonyTvControl.preChannel();
        mSonyTvControl.preChannel();
        mSonyTvControl.preChannel();
        mSonyTvControl.Onoff();

        System.out.println("==========================");

        //新遥控器测试
        NewTvControl mSharpTvControl;
        mSharpTvControl = new NewTvControl(new SharpControl());
        mSharpTvControl.Onoff();
        mSharpTvControl.nextChannel();
        mSharpTvControl.setChannel(9);
        mSharpTvControl.setChannel(28);
        mSharpTvControl.Back();
        mSharpTvControl.Onoff();
    }
}
