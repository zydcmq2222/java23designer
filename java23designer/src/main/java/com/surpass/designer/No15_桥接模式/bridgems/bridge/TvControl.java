package com.surpass.designer.No15_桥接模式.bridgems.bridge;

import com.surpass.designer.No15_桥接模式.bridgems.control.Control;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 21:19
 */
public class TvControl extends TvControlabs {

    private int ch = 0;
    private boolean ison = false;

    public TvControl(Control mControl) {
        super(mControl);
    }

    @Override
    public void Onoff() {
        if (ison) {
            ison = false;
            mControl.Off();
        } else {
            ison = true;
            mControl.On();
        }
    }

    @Override
    public void nextChannel() {
        ch++;
        mControl.setChannel(ch);
    }

    @Override
    public void preChannel() {
        ch--;
        if (ch < 0) {
            ch = 200;
        }
        mControl.setChannel(ch);
    }
}
