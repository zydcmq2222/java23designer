package com.surpass.designer.No15_桥接模式.bridgems.bridge;

import com.surpass.designer.No15_桥接模式.bridgems.control.Control;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 21:17
 */
public abstract class TvControlabs {
    Control mControl = null;

    public TvControlabs(Control mControl) {
        this.mControl = mControl;
    }

    public abstract void Onoff();

    public abstract void nextChannel();

    public abstract void preChannel();
}
