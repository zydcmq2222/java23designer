package com.surpass.designer.No21_备忘录模式.memento;

/**
 * 一个什么都没有的接口,操作者调用接口就不会知道具体任何信息
 * 这样设计提升信息安全
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/8 22:12
 */
public interface MementoIF {

}
