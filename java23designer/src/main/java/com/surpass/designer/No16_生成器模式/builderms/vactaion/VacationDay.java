package com.surpass.designer.No16_生成器模式.builderms.vactaion;

import java.util.ArrayList;
import java.util.Date;

/**
 * 每天度假详情
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 22:11
 */
public class VacationDay {
    private Date mDate;
    private String mHotels;
    private ArrayList<String> mTickets = null;
    private ArrayList<String> mEvents = null;

    public VacationDay(Date date) {
        mDate = date;
        mTickets = new ArrayList<>();
        mEvents = new ArrayList<>();
    }

    public void setDate(Date mDate) {
        this.mDate = mDate;
    }

    public void setHotel(String mHotels) {
        this.mHotels = mHotels;
    }

    public void addTicket(String ticket) {
        mTickets.add(ticket);
    }

    public void addEvent(String event) {
        mEvents.add(event);
    }

    public String showInfo() {
        StringBuilder stb = new StringBuilder();
        stb.append("Date:" + mDate.toString() + "\n");
        stb.append("Hotel:" + mHotels + "\n");
        stb.append("Tickets:" + mTickets.toString() + "\n");
        stb.append("Events:" + mEvents.toString() + "\n");
        return stb.toString();
    }
}
