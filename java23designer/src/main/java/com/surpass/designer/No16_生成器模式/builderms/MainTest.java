package com.surpass.designer.No16_生成器模式.builderms;

import com.surpass.designer.No16_生成器模式.builderms.builder.Builder3d;
import com.surpass.designer.No16_生成器模式.builderms.builder.Builder4d;
import com.surpass.designer.No16_生成器模式.builderms.builder.BuilderSelf;

/**
 * 度假项目主类:生成器模式,按照计划进行,不同计划生成不同对象
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 22:45
 */
public class MainTest {
    public static void main(String[] args) {

        //生成器省略抽象模式
        testself();
        System.out.println();

        Director mDirector = new Director(new Builder4d("2015-12-29"));
        mDirector.construct();

        mDirector.setBuilder(new Builder3d("2015-8-30"));
        mDirector.construct();
    }

    public static void testself() {
        BuilderSelf builder = new BuilderSelf("2015-9-29");

        builder.addTicket("Plane Ticket").addEvent("Fly to Destination")
                .addEvent("Supper").addHotel("Hilton");
        builder.addDay().addTicket("Zoo Ticket").addEvent("Bus to Zoo")
                .addEvent("Feed animals").addHotel("Home Inn");

        builder.addDay();
        builder.addTicket("Beach");
        builder.addEvent("Swimming");
        builder.addHotel("Home inn");

        builder.addDay().addTicket("Plane Ticket").addEvent("Fly to Home");
        builder.getVacation().showInfo();
    }
}

