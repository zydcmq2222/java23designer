package com.surpass.designer.No16_生成器模式.builderms.builder;

import com.surpass.designer.No16_生成器模式.builderms.vactaion.Vacation;

/**
 * 度假计划生成器
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 22:32
 */
public abstract class AbsBuilder {
    public Vacation mVacation;

    public AbsBuilder(String std) {
        mVacation = new Vacation(std);
    }

    public abstract void buildvacation();

    public abstract void buildDay(int i);

    public abstract void addHotel(String hotel);

    public abstract void addTicket(String ticket);

    public abstract void addEvent(String event);

    public Vacation getVacation() {
        return mVacation;
    }
}
