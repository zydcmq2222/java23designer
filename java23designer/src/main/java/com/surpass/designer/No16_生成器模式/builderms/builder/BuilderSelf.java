package com.surpass.designer.No16_生成器模式.builderms.builder;

import com.surpass.designer.No16_生成器模式.builderms.vactaion.Vacation;

/**
 * 用户DIY度假计划
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 23:05
 */
public class BuilderSelf {
    public Vacation mVacation;
    public BuilderSelf(String std) {
        mVacation = new Vacation(std);
    }

    public BuilderSelf addDay() {
        mVacation.addDay();
        return this;
    }

    public BuilderSelf buildDay(int i) {
        mVacation.setVacationDay(i);
        return this;
    }

    public BuilderSelf addHotel(String hotel) {
        mVacation.setHotel(hotel);
        return this;
    }

    public BuilderSelf addTicket(String ticket) {
        mVacation.addTicket(ticket);
        return this;
    }

    public BuilderSelf addEvent(String event) {
        mVacation.addEvent(event);
        return this;
    }

    public Vacation getVacation() {
        return mVacation;
    }


}
