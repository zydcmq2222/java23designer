package com.surpass.designer.No16_生成器模式.builderms.builder;

/**
 * 4天度假计划
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 22:36
 */
public class Builder4d extends AbsBuilder{

    public Builder4d(String std) {
        super(std);
    }

    @Override
    public void buildDay(int i) {
        mVacation.setVacationDay(i);
    }

    @Override
    public void addHotel(String hotel) {
        mVacation.setHotel(hotel);
    }

    @Override
    public void addTicket(String ticket) {
        mVacation.addTicket(ticket);
    }

    @Override
    public void addEvent(String event) {
        mVacation.addEvent(event);
    }

    @Override
    public void buildvacation() {
        addTicket("Plane Ticket");
        addEvent("Fly to Destination");
        addEvent("Supper");
        addHotel("Hilton");

        //第二天
        mVacation.addDay();
        addTicket("Zoo Ticket");
        addEvent("Bus to Zoo");
        addEvent("Feed animals");
        addHotel("Hilton");

        //第三天
        mVacation.addDay();
        addTicket("Beach");
        addEvent("Swimming");
        addHotel("Home inn");

        //第四天
        mVacation.addDay();
        addTicket("Plane Ticket");
        addEvent("Fly to Home");

    }
}
