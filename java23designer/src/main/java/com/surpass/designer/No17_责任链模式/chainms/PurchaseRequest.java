package com.surpass.designer.No17_责任链模式.chainms;

/**
 * 购买项目请求
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/5 20:59
 */
public class PurchaseRequest {
    private int Type = 0; //购买类型
    private int Number = 0;
    private float Price = 0;
    private int ID = 0; //请求ID号,由谁处理

    public PurchaseRequest(int Type, int Number, float Price) {
        this.Type = Type;
        this.Number = Number;
        this.Price = Price;
    }

    public int getType() {
        return Type;
    }

    public float GetSum() {
        return Number * Price;
    }

    public int GetID() {
        return (int) (Math.random() * 1000);
    }
}
