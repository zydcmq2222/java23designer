package com.surpass.designer.No17_责任链模式.chainms;

/**
 * 组长
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/5 21:04
 */
public class GroupApprover extends Approver{
    public GroupApprover(String Name) {
        super(Name + " GroupLeader");
    }

    @Override
    public void ProcessRequest(PurchaseRequest request) {
        if (request.GetSum() < 5000) {
            System.out.println("**This request " + request.GetID()
                    + " will be handled by " +
                    this.Name);
        } else {
            successor.ProcessRequest(request);
        }
    }
}

