package com.surpass.designer.No17_责任链模式.chainms;

/**
 * 责任链模式
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/5 21:16
 */
public class MainTest {
    public static void main(String[] args) {
        Client mClient = new Client();
        Approver GroupLeader = new GroupApprover("Tom");
        Approver DepartmentLeader = new DepartmentApprover("Jerry");
        Approver VicePresident = new VicePresidentApprover("Kate");
        Approver President = new PresidentApprover("Bush");

        //责任链,前后顺序改变,无影响
        GroupLeader.SetSuccessor(VicePresident);
        DepartmentLeader.SetSuccessor(President);
        VicePresident.SetSuccessor(DepartmentLeader);
        President.SetSuccessor(GroupLeader);

        VicePresident.ProcessRequest(mClient.sendRequest(1, 100, 40));
        VicePresident.ProcessRequest(mClient.sendRequest(2, 200, 40));
        VicePresident.ProcessRequest(mClient.sendRequest(3, 300, 40));
        VicePresident.ProcessRequest(mClient.sendRequest(4, 400, 140));

    }
}
