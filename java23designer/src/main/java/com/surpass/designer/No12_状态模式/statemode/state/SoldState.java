package com.surpass.designer.No12_状态模式.statemode.state;

/**
 * 售出状态
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/1 22:03
 */
public class SoldState implements State {
    private CandyMachine mCandyMachine;
    public SoldState(CandyMachine mCandyMachine) {
        this.mCandyMachine = mCandyMachine;
    }

    @Override
    public void insertCoin() {
        System.out.println("等待中,掉出糖果中");
    }

    @Override
    public void returnCoin() {
        System.out.println("已经转动把手,机器正在出售,无法退回!");
    }

    @Override
    public void trunCrank() {
        System.out.println("正在掉出糖果,你转多了也没啥卵用!");
    }

    @Override
    public void dispense() {
        mCandyMachine.releaseCandy();
        if (mCandyMachine.getCount() > 0) {
            mCandyMachine.setState(mCandyMachine.mOnReadyState);
        } else {
            System.out.println("没有糖果了");
            mCandyMachine.setState(mCandyMachine.mSoldOutState);
        }
    }

    @Override
    public void printstate() {
        System.out.println("***SoldState***");
    }
}
