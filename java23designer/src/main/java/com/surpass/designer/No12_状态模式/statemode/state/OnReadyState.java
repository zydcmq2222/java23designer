package com.surpass.designer.No12_状态模式.statemode.state;

/**
 * 待机状态
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/1 21:47
 */
public class OnReadyState implements State {

    private CandyMachine mCandyMachine;

    public OnReadyState(CandyMachine mCandyMachine) {
        this.mCandyMachine = mCandyMachine;
    }

    @Override
    public void insertCoin() {
        System.out.println("放入硬币成功,请转动把手");
        //状态改变
        mCandyMachine.setState(mCandyMachine.mHasCoin);
    }

    @Override
    public void returnCoin() {
        System.out.println("退回硬币中.....");
    }

    @Override
    public void trunCrank() {
        System.out.println("里面无硬币,转动把手失败!");
    }

    @Override
    public void dispense() {
        //待机状态,不分发糖果
    }

    @Override
    public void printstate() {
        System.out.println("***OnReadyState***");
    }
}
