package com.surpass.designer.No12_状态模式.statemode.state;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/1 21:34
 */
interface State {
    public void insertCoin();
    public void returnCoin();
    public void trunCrank();
    public void dispense(); //分发糖果
    public void printstate();
}
