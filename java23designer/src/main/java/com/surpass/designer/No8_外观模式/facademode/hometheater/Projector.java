package com.surpass.designer.No8_外观模式.facademode.hometheater;

/**
 * 投影仪
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 16:23
 */
public class Projector {
    private int size = 5;
    private static Projector instance = null;

    private Projector(){}

    public static Projector getInstance() {
        if (instance == null) {
            instance = new Projector();
        }
        return instance;
    }

    public void on(){
        System.out.println("Projector On");
    }

    public void off(){
        System.out.println("Projector Off");
    }

    public void focus(){
        System.out.println("Projector is focus");
    }

    public void zoom(int size) {
        this.size = size;
        System.out.println("Projector zoom to " + size);
    }

}
