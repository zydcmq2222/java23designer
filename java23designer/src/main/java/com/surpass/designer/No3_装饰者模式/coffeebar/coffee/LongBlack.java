package com.surpass.designer.No3_装饰者模式.coffeebar.coffee;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 22:19
 */
public class LongBlack extends Coffee{
    public LongBlack(){
        super.setDescription("LongBlack");
        super.setPrice(10.0f);
    }
}
