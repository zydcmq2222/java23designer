package com.surpass.designer.No3_装饰者模式.coffeebar;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 22:10
 */
public abstract class Drink {
    public String description = ""; //描述
    private float price = 0f; //价格

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description + "-" + this.getPrice();
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getPrice() {
        return price;
    }

    //调料不能单独存在,其cost与主体递归调用计算价格,采用抽象方法
    public abstract float cost();

}
