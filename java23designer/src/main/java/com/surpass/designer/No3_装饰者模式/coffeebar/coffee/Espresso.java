package com.surpass.designer.No3_装饰者模式.coffeebar.coffee;

/**
 * 咖啡的一种
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 22:19
 */
public class Espresso extends Coffee {
    public Espresso(){
        super.setDescription("Espresso");
        super.setPrice(4.0f);
    }
}
