package com.surpass.designer.No4_单例模式.singleton;

/**
 * 原始巧克力工厂,目的:工厂1和2使用同一个锅炉
 * 以下方案new 一个工厂就会新建一个锅炉,状态混乱
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 14:44
 */
public class ChocolateFactory {
    private boolean empty; //空的
    private boolean boiled;//是否加热

    public ChocolateFactory(){
        empty = true;
        boiled = false;
    }

    public void fill(){
        if(empty){
            //添加原料动作
            empty = false;
            boiled = false;
        }
    }

    public void drain(){
        if ((!empty) && boiled) {
            //排出巧克力
            empty = true;
        }
    }

    public void boil(){
        if ((!empty) && (!boiled)) {
            //加热
            boiled = true;
        }
    }
}
