package com.surpass.designer.No4_单例模式.singleton;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 14:42
 */
public class Singleton {
    private static Singleton uniqeInstance = null;

    private Singleton(){}

    public static Singleton getInstance(){
        if (uniqeInstance == null) {
            uniqeInstance = new Singleton();
        }
        return uniqeInstance;
    }
}
