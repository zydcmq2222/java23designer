package com.surpass.designer.No13_代理模式.agentmode.dyn;

import java.lang.reflect.Proxy;

/**
 * 服务,自己访问自己和他人访问自己的权限设置
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/3 21:02
 */
public class MatchService {
    public MatchService() {
        PersonBean joe = getPersonInfo("joe", "male", "running");

        //自己访问自己,不可以设置分数
        PersonBean ownerProxy = getOwnerProxy(joe);

        System.out.println("Name is " + ownerProxy.getName());
        System.out.println("Interests is " + ownerProxy.getInterests());

        ownerProxy.setInterests("Bowling");
        System.out.println("Interests are " + ownerProxy.getInterests());
        ownerProxy.setHotOrNotRating(50);
        System.out.println("Rating is " + ownerProxy.getHotOrNotRating());
        ownerProxy.setHotOrNotRating(40);
        System.out.println("Rating is " + ownerProxy.getHotOrNotRating());

        System.out.println("8*************8");

        //他人访问自己,不可以设置姓名性别兴趣
        PersonBean nonOwnerProxy = getNonOwnerProxy(joe);
        System.out.println("Name is " + nonOwnerProxy.getName());
        nonOwnerProxy.setInterests("Haha");
        System.out.println("Interests are " + nonOwnerProxy.getInterests());
        nonOwnerProxy.setHotOrNotRating(60);
        System.out.println("Rating is " + nonOwnerProxy.getHotOrNotRating());

        System.out.println();


    }

    //获取人物信息
    PersonBean getPersonInfo(String name, String gender, String intrests) {
        PersonBean person = new PersonBeanImpl();
        person.setName(name);
        person.setGender(gender);
        person.setInterests(intrests);
        return person;
    }

    //获取自己访问自己代理
    PersonBean getOwnerProxy(PersonBean person) {
        return (PersonBean) Proxy.newProxyInstance(person.getClass().getClassLoader(),
                person.getClass().getInterfaces(),
                new OwnerInvocationHandler(person));
    }

    //获取他人访问自己代理
    PersonBean getNonOwnerProxy(PersonBean person) {
        return (PersonBean) Proxy.newProxyInstance(person.getClass().getClassLoader(),
                person.getClass().getInterfaces(),
                new NonOwnerInvocationHandler(person));
    }
}
