package com.surpass.designer.No13_代理模式.agentmode.candymachine;

import java.util.Random;

/**
 * 有硬币状态
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/1 21:56
 */
public class HasCoin implements State {
    private CandyMachine mCandyMachine;
    public HasCoin(CandyMachine mCandyMachine) {
        this.mCandyMachine = mCandyMachine;
    }

    @Override
    public void insertCoin() {
        System.out.println("已有硬币!不能再放入硬币");
    }

    @Override
    public void returnCoin() {
        System.out.println("退回硬币中.....");
        mCandyMachine.setState(mCandyMachine.mOnReadyState);
    }

    @Override
    public void trunCrank() {
        System.out.println("转动把手中");
        //增加赢家状态,随机函数
        Random ranwinner = new Random();
        int winner = ranwinner.nextInt(10);//0 - 9
        if (winner == 0) {
            mCandyMachine.setState(mCandyMachine.mWinnerState);
        } else {
            mCandyMachine.setState(mCandyMachine.mSoldState);
        }

    }

    @Override
    public void dispense() {
        //
    }

    @Override
    public void printstate() {
        System.out.println("***HasCoin***");
    }
}
