package com.surpass.designer.No13_代理模式.agentmode.dyn;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 其他用户给自己打分,但不允许设置自己姓名性别爱好
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/3 0:01
 */
public class NonOwnerInvocationHandler implements InvocationHandler {

    PersonBean person;

    public NonOwnerInvocationHandler(PersonBean person) {
        this.person = person;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().startsWith("get")) {
            return method.invoke(person, args);
        } else if (method.getName().equals("setHotOrNotRating")) {
            return method.invoke(person, args);
        } else if (method.getName().startsWith("set")) {
            //不允许调用打分以外其他方法
            return new IllegalAccessException();
        }
        return null;
    }
}
