package com.surpass.designer.No13_代理模式.agentmode.dyn;

/**
 * 代理项目,设置代理,使得:
 * 自己无法给自己调用打分方法,但可以给自己设置名字性别爱好
 * 他人无法设置自己的姓名性别爱好,但是可以给自己打分
 * 由于类方法全是public,任何类都可以修改.为实现以上需求
 * 引入代理模式--保护代理
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/3 21:14
 */
public class MainTest {
    public static void main(String[] args) {
        new MatchService();
    }
}
