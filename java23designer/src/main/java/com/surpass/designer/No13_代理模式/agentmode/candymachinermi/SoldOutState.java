package com.surpass.designer.No13_代理模式.agentmode.candymachinermi;



/**
 * 售罄状态
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/1 22:07
 */
public class SoldOutState implements State {
    private transient CandyMachine mCandyMachine;
    public SoldOutState(CandyMachine mCandyMachine) {
        this.mCandyMachine = mCandyMachine;
    }

    @Override
    public void insertCoin() {
        System.out.println("售罄,不能放硬币");
    }

    @Override
    public void returnCoin() {
        System.out.println("机器售罄状态,无法退还!");
    }

    @Override
    public void trunCrank() {
        System.out.println("成功转动把手,但是里面没有糖果了");
    }

    @Override
    public void dispense() {

    }

    @Override
    public void printstate() {
        System.out.println("***SoldOutState***");
    }
}
