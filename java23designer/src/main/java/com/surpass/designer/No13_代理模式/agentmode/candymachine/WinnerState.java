package com.surpass.designer.No13_代理模式.agentmode.candymachine;


/**
 * 赢家状态
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/1 22:09
 */
public class WinnerState implements State {
    private CandyMachine mCandyMachine;
    public WinnerState(CandyMachine mCandyMachine) {
        this.mCandyMachine = mCandyMachine;
    }

    @Override
    public void insertCoin() {
        System.out.println("等待中,掉出糖果中");
    }

    @Override
    public void returnCoin() {
        System.out.println("已经转动把手,机器正在出售,无法退回!");
    }

    @Override
    public void trunCrank() {
        System.out.println("正在掉出糖果,你转多了也没啥卵用!");
    }

    @Override
    public void dispense() {
        //赢家状态,分发2粒糖果
        mCandyMachine.releaseCandy(); //减少一粒糖果
        if (mCandyMachine.getCount() == 0) { //没有糖果
            //用户中奖,但是机器没有另外的可以分发的糖果,不告知用户赢家状态
            mCandyMachine.setState(mCandyMachine.mSoldOutState);
        } else {
            System.out.println("恭喜你中奖了,给你另外一个果砸!");
            mCandyMachine.releaseCandy();
            if (mCandyMachine.getCount() > 0) {
                mCandyMachine.setState(mCandyMachine.mOnReadyState);
            } else {
                System.out.println("没有糖果了");
                mCandyMachine.setState(mCandyMachine.mSoldOutState);
            }
        }
    }

    @Override
    public void printstate() {
        System.out.println("***WinnerState***");
    }
}
