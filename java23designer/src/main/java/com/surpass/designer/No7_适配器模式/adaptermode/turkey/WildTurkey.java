package com.surpass.designer.No7_适配器模式.adaptermode.turkey;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/10 21:59
 */
public class WildTurkey implements Turkey {
    @Override
    public void gobble() {
        System.out.println("Go Go");
    }

    @Override
    public void fly() {
        System.out.println("I am flying a short distance");
    }
}
