package com.surpass.designer.No7_适配器模式.adaptermode.duck;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/10 21:57
 */
public interface Duck {
    public void quack();//鸭子叫
    public void fly();
}
