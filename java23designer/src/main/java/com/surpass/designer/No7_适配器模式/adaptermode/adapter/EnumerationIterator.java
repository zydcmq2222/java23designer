package com.surpass.designer.No7_适配器模式.adaptermode.adapter;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * 低版本java枚举器与高版本java迭代器的适配
 * 自定义适配器
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/10 22:36
 */
public class EnumerationIterator implements Iterator<Object>{

    private Enumeration enumeration;

    public EnumerationIterator(Enumeration enumeration) {
        this.enumeration = enumeration;
    }

    @Override
    public boolean hasNext() {
        return enumeration.hasMoreElements();
    }

    @Override
    public Object next() {
        return enumeration.nextElement();
    }

    @Override
    public void remove() {
        //对外看来是一个迭代器,有remove方法
        //但对内因为要调用枚举器对应的方法,其没有该方法,所以无法实现
        throw new UnsupportedOperationException();
    }
}
