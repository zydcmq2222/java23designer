package com.surpass.designer.No2_观察者模式.internetweather.mode;

import com.surpass.designer.No2_观察者模式.internetweather.observer.Observer;
import com.surpass.designer.No2_观察者模式.internetweather.observer.Subject;

import java.util.ArrayList;

/**
 * 实现如何保存,移除观察者
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 19:12
 */
public class WeatherDataSt implements Subject{
    private float mTemperature; //温度
    private float mPressure; //气压
    private float mHumidity; //湿度

    private ArrayList<Observer> mObservers;

    public WeatherDataSt() {
        mObservers = new ArrayList<>();
    }

    public float getmTemperature() {
        return mTemperature;
    }

    public float getmPressure() {
        return mPressure;
    }

    public float getmHumidity() {
        return mHumidity;
    }

    public void dataChange(){
        notifyObservers();
    }


    //模拟气象站传输数据
    public void setData(float mTemperature, float mPressure, float mHumidity) {
        this.mTemperature = mTemperature;
        this.mPressure = mPressure;
        this.mHumidity = mHumidity;
        dataChange();
    }

    @Override
    public void registrObserver(Observer o) {
        mObservers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        if(mObservers.contains(o)){
            mObservers.remove(o);
        }
    }

    @Override
    public void notifyObservers() {
        for(int i = 0,len = mObservers.size();i<len;i++) {
            mObservers.get(i).update(getmTemperature(),getmPressure(),getmHumidity());
        }
    }
}
