package com.surpass.designer.No2_观察者模式.internetweather.observer;

/**
 * 注册观察者
 * 移除观察者
 * 通知注册的观察者
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 19:06
 */
public interface Subject {
    public void registrObserver(Observer o);
    public void removeObserver(Observer o);
    public void notifyObservers();
}
