package com.surpass.designer.No2_观察者模式.internetweather.mode;

import com.surpass.designer.No2_观察者模式.internetweather.observer.Observer;

/**
 * B公司的公告板
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 19:08
 */
public class ForcastConditions implements Observer{

    private float mTemperature;
    private float mPressure;
    private float mHumidity;

    @Override
    public void update(float mTemperature, float mPressure, float mHumidity) {
        this.mTemperature = mTemperature;
        this.mPressure = mPressure;
        this.mHumidity = mHumidity;
        display();
    }

    public void display(){
        System.out.println("***明天温度:" + (mTemperature + Math.random()) + "***");
        System.out.println("***明天气压:" + (mPressure + Math.random()) + "***");
        System.out.println("***明天湿度:" + (mHumidity + Math.random()) + "***");
    }
}
