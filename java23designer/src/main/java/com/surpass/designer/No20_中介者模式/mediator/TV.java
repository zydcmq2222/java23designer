package com.surpass.designer.No20_中介者模式.mediator;

/**
 * 同事TV
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/8 21:34
 */
public class TV extends Colleague {

    public TV(Mediator mediator, String name) {
        super(mediator, name);
        mediator.Register(name,this);
    }

    @Override
    public void SendMessage(int stateChange) {
        this.GetMediator().GetMessage(stateChange, this.name);
    }

    public void StartTV() {
        System.out.println("It's time to StartTV!");
    }

    public void StopTV() {
        System.out.println("StopTv!");
    }
}
