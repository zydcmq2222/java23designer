package com.surpass.designer.No20_中介者模式.mediator;

/**
 * 中介者模式主测试函数
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/8 21:42
 */
public class MainTest {
    public static void main(String[] args) {
        Mediator mediator = new ConcreteMediator();

        //闹铃
        Alarm mAlarm = new Alarm(mediator, "mAlarm");
        //咖啡机
        CoffeeMachine mCoffeeMachine = new CoffeeMachine(mediator, "mCoffeeMachine");
        //窗帘
        Curtains mCurtains = new Curtains(mediator, "mCurtains");
        TV mTV = new TV(mediator, "mTV");
        mAlarm.SendAlarm(0);
        mCoffeeMachine.FinishCoffee();
        mAlarm.SendAlarm(1);
    }
}
