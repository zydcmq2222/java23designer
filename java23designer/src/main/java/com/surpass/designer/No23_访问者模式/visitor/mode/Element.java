package com.surpass.designer.No23_访问者模式.visitor.mode;

/**
 * 元素,基类
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/9 23:20
 */
public abstract class Element {
    //所有子类能够接收访问者
    abstract public void Accept(Visitor visitor);
}
