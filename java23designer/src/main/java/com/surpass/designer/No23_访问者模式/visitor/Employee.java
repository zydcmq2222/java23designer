package com.surpass.designer.No23_访问者模式.visitor;

/**
 * 员工类
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/9 23:01
 */
public class Employee {
    private String name;
    private float income; //收入
    private int vacationDays; //年假
    private int degree; //级别

    public Employee(String name, float income, int vacationDays, int degree) {
        this.name = name;
        this.income = income;
        this.vacationDays = vacationDays;
        this.degree = degree;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getIncome() {
        return income;
    }

    public void setIncome(float income) {
        this.income = income;
    }

    public int getVacationDays() {
        return vacationDays;
    }

    public void setVacationDays(int vacationDays) {
        this.vacationDays = vacationDays;
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }
}
