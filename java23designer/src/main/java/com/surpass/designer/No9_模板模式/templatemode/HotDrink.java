package com.surpass.designer.No9_模板模式.templatemode;

/**
 * 抽取一模一样的功能到超类,代码复用
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 17:18
 */
public abstract class HotDrink {

    public abstract void prepareRecipe();

    //相同部分放超类
    public void boilWater() {
        System.out.println("Boiling water");
    }

    public void pourInCup() {
        System.out.println("Pouring into cup");
    }
}
