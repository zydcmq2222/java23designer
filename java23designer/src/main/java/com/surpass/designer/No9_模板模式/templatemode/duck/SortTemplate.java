package com.surpass.designer.No9_模板模式.templatemode.duck;

/**
 * 模板模式:排序模板
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 17:51
 */
public abstract class SortTemplate {
    public final void sort(Object[] objects) {

        for (int i = 0; i < objects.length; i++) {
            //比较,让子类实现
            if (compare(objects[i + 1]) > 0) {
                //swap();
            } else {

            }
        }
    }

    public abstract int compare(Object object);
}
