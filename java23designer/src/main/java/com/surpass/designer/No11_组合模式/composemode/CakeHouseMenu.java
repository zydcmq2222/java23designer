package com.surpass.designer.No11_组合模式.composemode;


import java.util.ArrayList;
import java.util.Iterator;

/**
 * 蛋糕店使用ArrayList管理菜单
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/25 23:17
 */
public class CakeHouseMenu extends MenuComponent{
    private ArrayList<MenuComponent> menuItems;

    public CakeHouseMenu() {
        menuItems = new ArrayList<>();

        addItem("KFC Cake Breakfast", "boiled eggs&toast&cabbage", true, 3.99f);
        addItem("MDL Cake Breakfast", "fried eggs&toast", false, 3.59f);
        addItem("Stawberry Cake", "fresh stawberry", true, 3.29f);
        addItem("Regular Cake Breakfast", "toast&sausage", true, 2.59f);
    }

    private void addItem(String name, String description, boolean vegetable,
                         float price) {
        MenuComponent menuItem = new MenuItem(name, description, vegetable, price);
        menuItems.add(menuItem);
    }

    //主菜单输出
    @Override
    public void print() {
        System.out.println("***This is CakeHouseMenu***");
    }

    public Iterator getIterator() {
        return new ComposeIterator(menuItems.iterator());
    }


}
