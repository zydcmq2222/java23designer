package com.surpass.designer.No11_组合模式.composemode;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/28 21:25
 */
public class MainTest {
    public static void main(String[] args) {
        Waitress mWaitress = new Waitress();
        CakeHouseMenu mCakeHouseMenu = new CakeHouseMenu();
        DinerMenu mDinerMenu = new DinerMenu();
        mWaitress.addComponent(mCakeHouseMenu);
        mWaitress.addComponent(mDinerMenu);
        //打印所有菜单
        mWaitress.printMenu();
        //打印素食菜单
        mWaitress.printVegetableMenu();
    }
}
