package com.surpass.designer.No11_组合模式.composemode;

import java.util.Iterator;

/**
 * 超类:菜单和菜单项
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/26 23:30
 */
public abstract class MenuComponent {
    public String getName() { //菜单名字
        return "";
    }

    public String getDescription() {
        return "";
    }

    public float getPrice() { //价格
        return 0;
    }

    public boolean isVegetable() {
        return false;
    }

    public abstract void print();

    public Iterator getIterator() {
        return new NullIterator();
    }

}
