package com.surpass.designer.No5_工厂模式.pizzastore.absfactory;

import com.surpass.designer.No5_工厂模式.pizzastore.pizza.LDCheesePizza;
import com.surpass.designer.No5_工厂模式.pizzastore.pizza.LDPepperPizza;
import com.surpass.designer.No5_工厂模式.pizzastore.pizza.Pizza;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 16:22
 */
public class LDFactory implements AbsFactory{
    @Override
    public Pizza CreatePizza(String ordertype) {
        Pizza pizza = null;
        if (ordertype.equals("cheese")) {
            pizza = new LDCheesePizza();
        } else if (ordertype.equals("pepper")) {
            pizza = new LDPepperPizza();
        }
        return pizza;
    }
}
