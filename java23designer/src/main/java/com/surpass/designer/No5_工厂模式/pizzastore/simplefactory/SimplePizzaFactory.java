package com.surpass.designer.No5_工厂模式.pizzastore.simplefactory;

import com.surpass.designer.No5_工厂模式.pizzastore.pizza.*;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 15:40
 */

public class SimplePizzaFactory {
    public Pizza CreatePizza(String ordertype) {
        Pizza pizza = null;
        if (ordertype.equals("cheese")) {
            pizza = new CheesePizza(); //多地工厂,B地pizza为CCPizza,则100个工厂需要100个不同的类
        } else if (ordertype.equals("greek")) {
            pizza = new GreekPizza();
        } else if (ordertype.equals("pepper")) {
            pizza = new PepperPizza();
        }
        return pizza;
    }
}