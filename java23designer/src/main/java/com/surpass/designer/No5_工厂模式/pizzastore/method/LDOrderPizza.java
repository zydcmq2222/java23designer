package com.surpass.designer.No5_工厂模式.pizzastore.method;

import com.surpass.designer.No5_工厂模式.pizzastore.pizza.LDCheesePizza;
import com.surpass.designer.No5_工厂模式.pizzastore.pizza.LDPepperPizza;
import com.surpass.designer.No5_工厂模式.pizzastore.pizza.Pizza;

/**
 * 伦敦pizza
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 16:06
 */
public class LDOrderPizza extends OrderPizza{

    @Override
    Pizza createPizza(String ordertype) {
        Pizza pizza = null;
        if (ordertype.equals("cheese")) {
            pizza = new LDCheesePizza();
        } else if (ordertype.equals("pepper")) {
            pizza = new LDPepperPizza();
        }
        return pizza;
    }
}
