package com.surpass.designer.No5_工厂模式.pizzastore.pizza;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 15:22
 */
public class LDPepperPizza extends Pizza {
    @Override
    public void prepare() {
        super.setName("LDPepperPizza");
        System.out.println(name + " preparing");
    }
}
