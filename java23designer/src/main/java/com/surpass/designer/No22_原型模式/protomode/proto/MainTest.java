package com.surpass.designer.No22_原型模式.protomode.proto;

import com.surpass.designer.No22_原型模式.protomode.EventTemplate;

import java.util.Random;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/9 22:21
 */
public class MainTest {
    public static void main(String[] args) {
        int i = 0;
        int MAX_COUNT = 10;
        EventTemplate et = new EventTemplate("9月份信用卡账单", "国庆抽奖活动...");

        Mail mail = new Mail(et);

        while (i < MAX_COUNT) {
            //以下是每封邮件不同的地方
            //内存复制,新对象制造,新对象在新的内存空间,同时,cloneMail和mail的 ArrayList 对象
            //不会拷贝,指向同一个内存空间,内存不安全
            //解决办法:clone重写方法再拷贝一次ArrayList,深拷贝
            Mail cloneMail = mail.clone();
            cloneMail.setContent(getRandString(5) + ",先生(女士): 你的信用卡账单..." +
                    mail.getTail());
            cloneMail.setReceiver(getRandString(5) + "@" + getRandString(8) + ".com");
            //发送邮件
            sendMail(cloneMail);
            i++;
        }


    }

    //随机产生用户
    public static String getRandString( int maxLength) {
        String source = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuffer sb = new StringBuffer();
        Random rand = new Random();
        for (int i = 0; i < maxLength; i++) {
            sb.append(source.charAt(rand.nextInt(source.length())));
        }
        return sb.toString();
    }

    public static void sendMail(Mail mail) {
        System.out.println("标题:" + mail.getSubject() + "\t收件人:" +
                mail.getReceiver() + "\t内容:" + mail.getContent() + "\t....发送成功!");
    }
}
