package com.surpass.designer.No18_蝇量模式.flyweight;

/**
 * 传统模式构造对象内存消耗测试
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/5 22:02
 */
public class MainTest {
    public static void main(String[] args) {
        showMemInfo();
        TreesTest mTreesSet;
        mTreesSet = new TreesTest();

        showMemInfo();
        mTreesSet.display();
        showMemInfo();
    }

    public static void showMemInfo() {
        //最大内存
        long max = Runtime.getRuntime().maxMemory();
        //分配内存
        long total = Runtime.getRuntime().totalMemory();
        //已分配内存中的剩余空间
        long free = Runtime.getRuntime().freeMemory();
        //已占用内存
        long used = total - free;

        System.out.println("最大内存= " + max);
        System.out.println("已分配内存= " + total);
        System.out.println("已分配内存中的剩余空间= " + free);
        System.out.println("已占用内存= " + used);
        System.out.println("时间= " + System.currentTimeMillis());
        System.out.println("");
    }
}

/*
传统方式内存占用

最大内存= 4746379264
已分配内存= 321912832
已分配内存中的剩余空间= 315159832
已占用内存= 6753000
时间= 1562335619057

最大内存= 4746379264
已分配内存= 567279616
已分配内存中的剩余空间= 284458928
已占用内存= 282820688
时间= 1562335623147

最大内存= 4746379264
已分配内存= 567279616
已分配内存中的剩余空间= 284458928
已占用内存= 282820688
时间= 1562335623157
 */
