package com.surpass.designer.No1_策略模式.stimulateduck.flybehavior;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 17:20
 */
public interface FlyBehavior {
    void fly();
}
