package com.surpass.designer.No1_策略模式.stimulateduck;


import com.surpass.designer.No1_策略模式.stimulateduck.duck.Duck;
import com.surpass.designer.No1_策略模式.stimulateduck.duck.GreenHeadDuck;
import com.surpass.designer.No1_策略模式.stimulateduck.duck.RedHeadDuck;
import com.surpass.designer.No1_策略模式.stimulateduck.flybehavior.NoFlyBehavior;
import com.surpass.designer.No1_策略模式.stimulateduck.quackbehavior.NoQuackBehavior;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 17:38
 */
public class StimulateDuck {

    public static void main(String[] args) {
        Duck mGreenHeadDuck = new GreenHeadDuck();
        Duck mRedHeadDuck = new RedHeadDuck();

        mGreenHeadDuck.display();
        mGreenHeadDuck.Fly();
        mGreenHeadDuck.Quack();
        mGreenHeadDuck.swim();

        mRedHeadDuck.display();
        mRedHeadDuck.Fly();
        mRedHeadDuck.Quack();
        mRedHeadDuck.swim();
        mRedHeadDuck.display();

        System.out.println("====================");

        //动态改变对象行为
        mRedHeadDuck.SetFlyBehavior(new NoFlyBehavior());
        mRedHeadDuck.Fly();

        mRedHeadDuck.SetQuackBehavior(new NoQuackBehavior());
        mRedHeadDuck.Quack();

    }
}
