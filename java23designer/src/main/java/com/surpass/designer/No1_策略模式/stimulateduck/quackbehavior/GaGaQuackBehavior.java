package com.surpass.designer.No1_策略模式.stimulateduck.quackbehavior;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 17:23
 */
public class GaGaQuackBehavior implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("--GaGa--");
    }
}
