package com.surpass.designer.No1_策略模式.stimulateduck.duck;

import com.surpass.designer.No1_策略模式.stimulateduck.flybehavior.BadFlyBehavior;
import com.surpass.designer.No1_策略模式.stimulateduck.quackbehavior.GuGuQuackBehavior;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 17:36
 */
public class RedHeadDuck extends Duck{

    public RedHeadDuck(){
        mFlyBehavior = new BadFlyBehavior();
        mQuackBehavior = new GuGuQuackBehavior();
    }

    @Override
    public void display() {
        System.out.println("**RedHead**");
    }
}
