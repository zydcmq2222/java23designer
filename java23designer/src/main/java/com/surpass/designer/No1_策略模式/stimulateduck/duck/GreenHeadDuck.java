package com.surpass.designer.No1_策略模式.stimulateduck.duck;

import com.surpass.designer.No1_策略模式.stimulateduck.flybehavior.GoodFlyBehavior;
import com.surpass.designer.No1_策略模式.stimulateduck.quackbehavior.GaGaQuackBehavior;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 17:29
 */
public class GreenHeadDuck extends Duck {

    //行为组合,绿头鸭会好飞行,嘎嘎叫
    public GreenHeadDuck(){
        //超类继承
        mFlyBehavior = new GoodFlyBehavior();
        mQuackBehavior = new GaGaQuackBehavior();
    }

    @Override
    public void display() {
        System.out.println("**GreenHead**");
    }

}
