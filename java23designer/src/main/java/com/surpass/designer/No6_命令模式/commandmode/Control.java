package com.surpass.designer.No6_命令模式.commandmode;

/**
 * 控制器
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 22:20
 */
public interface Control {
    public void onButton(int slot);
    public void offButton(int slot);
    public void undoButton();
}
