package com.surpass.designer.No6_命令模式.commandmode;

import com.surpass.designer.No6_命令模式.commandmode.device.Light;
import com.surpass.designer.No6_命令模式.commandmode.device.Stereo;

/**
 * 传统遥控器方案
 * 存在的问题:当新设备进来,设备对象添加,构造方法增加新设备
 * on/offButton方法case 新增加
 * 耦合度过高,维护及其困难
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 22:22
 */
public class TraditionControl implements Control{
    //TraditionControl控制器控制灯和音响
    Light light;
    Stereo stereo;

    public TraditionControl(Light light, Stereo stereo) {
        this.light = light;
        this.stereo = stereo;
    }

    @Override
    public void onButton(int slot) {
        switch (slot) {
            case 0://第一个插槽,灯亮
                light.On();
                break;
            case 1://第二个插槽,音响打开
                stereo.On();
                break;
            case 2://第三个插槽,增加音量
                int vol = stereo.GetVol();
                if(vol < 11){
                    stereo.SetVol(++vol);
                }
                break;
        }
    }

    @Override
    public void offButton(int slot) {
        switch (slot) {
            case 0://第一个插槽,灯灭
                light.Off();
                break;
            case 1://第二个插槽,音响关闭
                stereo.Off();
                break;
            case 2://第三个插槽,减小音量
                int vol = stereo.GetVol();
                if(vol > 0){
                    stereo.SetVol(--vol);
                }
                break;
        }
    }

    @Override
    public void undoButton() {

    }
}
