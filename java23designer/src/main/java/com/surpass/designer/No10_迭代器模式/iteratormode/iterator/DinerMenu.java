package com.surpass.designer.No10_迭代器模式.iteratormode.iterator;

import com.surpass.designer.No10_迭代器模式.iteratormode.MenuItem;

/**
 * 中餐厅使用数组管理菜单,迭代模式
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/25 23:24
 */
public class DinerMenu {
    private final static int Max_Item = 5; //最多5个菜单项
    public int numberOfItems = 0;
    private MenuItem[] menuItems;

    public DinerMenu() {
        menuItems = new MenuItem[Max_Item];
        addItem("vegetable Blt", "bacon&lettuce&tomato&cabbage", true, 3.58f);
        addItem("Blt", "bacon&lettuce&tomato", false, 3.00f);
        addItem("bean soup", "bean&potato salad", true, 3.28f);
        addItem("hotdog", "onions&cheese&bread", false, 3.05f);
    }

    private void addItem(String name, String description, boolean vegetable,
                         float price) {
        MenuItem menuItem = new MenuItem(name, description, vegetable, price);
        if (numberOfItems >= Max_Item) {
            System.err.println("sorry,menu is full! can not add another item");
        }else {
            menuItems[numberOfItems] = menuItem;
            numberOfItems++;
        }

    }

    public Iterator getIterator() {
        return new DinerIterator();
    }

    class DinerIterator implements Iterator {
        private int position;

        public DinerIterator() {
            position = 0;
        }
        @Override
        public boolean hasNext() {
            if (position < numberOfItems) {
                return true;
            }
            return false;
        }

        @Override
        public Object next() {
            MenuItem menuItem = menuItems[position];
            position++;
            return menuItem;
        }
    }


}
