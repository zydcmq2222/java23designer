package com.surpass.designer.No10_迭代器模式.iteratormode.java;


/**
 * 测试两个合并的餐厅又合并一家咖啡馆,使用java内置迭代器
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/26 21:26
 */
public class MainTest {
    public static void main(String[] args) {
        Waitress mWaitress = new Waitress();
        CakeHouseMenu mCakeHouseMenu = new CakeHouseMenu();
        DinerMenu mDinerMenu = new DinerMenu();
        CafeMenu mCafeMenu = new CafeMenu();

        //迭代器只负责遍历,不一定会按照顺序遍历

        //蛋糕店迭代器
        mWaitress.addIterator(mCakeHouseMenu.getIterator());
        //中餐厅迭代器
        mWaitress.addIterator(mDinerMenu.getIterator());
        //咖啡迭代器
        mWaitress.addIterator(mCafeMenu.getIterator());
        mWaitress.printMenu();
    }


}
