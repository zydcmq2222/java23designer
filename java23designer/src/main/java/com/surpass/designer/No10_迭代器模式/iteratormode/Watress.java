package com.surpass.designer.No10_迭代器模式.iteratormode;

import java.util.ArrayList;

/**
 * 女招待,两个餐厅合并成一个餐厅,模拟顾客点餐
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/25 23:31
 */
public class Watress {
    private CakeHouseMenu mCakeHouseMenu;
    private DinerMenu mDinerMenu;
    private ArrayList<MenuItem> cakeitems;
    private MenuItem[] dineritems;

    public Watress() {
        mCakeHouseMenu = new CakeHouseMenu();
        //获取暴露的菜单
        cakeitems = mCakeHouseMenu.getMenuItems();

        mDinerMenu = new DinerMenu();
        dineritems = mDinerMenu.getMenuItems();
    }

    //打印菜单功能
    public void printMenu() {
        MenuItem menuItem;
        //ArrayList
        for(int i = 0;i < cakeitems.size();i++) {
            menuItem = cakeitems.get(i);
            System.out.println(menuItem.getName() + "***" +
                    menuItem.getPrice() + "***" + menuItem.getDescription());
        }
        //数组
        for(int i = 0;i< mDinerMenu.numberOfItems;i++) {
            menuItem = dineritems[i];
            System.out.println(menuItem.getName() + "***" +
                    menuItem.getPrice() + "***" + menuItem.getDescription());
        }
    }
}
