package com.surpass.designer.No10_迭代器模式.iteratormode.java;

import com.surpass.designer.No10_迭代器模式.iteratormode.MenuItem;

import java.util.Hashtable;
import java.util.Iterator;

/**
 * 咖啡馆菜单,使用Hashtable维护
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/26 21:56
 */
public class CafeMenu {
    private Hashtable<String, MenuItem> menuItems = new Hashtable<>();

    public CafeMenu() {
        addItem("Moca Burger", "moca&bruger&tomato", true, 3.56f);
        addItem("Soup Latte", "Latte&salad&soup", true, 3.26f);
        addItem("Burrito", "bacon&toamto&beans", false, 3.96f);
    }

    private void addItem(String name, String description, boolean vegetable,
                         float price) {
        MenuItem menuItem = new MenuItem(name, description, vegetable, price);
        menuItems.put(name, menuItem);
    }

    public Iterator getIterator() {
        return menuItems.values().iterator();
    }
}
